const { Client } = require("@notionhq/client")
const dotenv = require('dotenv');
dotenv.config({path:'variables.env'});
const fs = require('fs');


const notion = new Client({
    auth : process.env.NOTION_TOKEN
})
let task = [];
const date = new Date();
const dateNow = date.toLocaleDateString('en-CA');
console.log("dateNow",dateNow);
const databaseId = process.env.NOTION_DATABASEID;
const pathExport = `${process.env.PATH_EXPORT}/${dateNow}.daily.txt`;
const headerSection = `-- Daily Report ${dateNow} --\n\n## Actividades ##\n`;
task.push(headerSection);
;(async()=> {
    const response = await notion.databases.query({
        database_id: databaseId,
        filter: {
            property: "dateTask",
            date: {
                equals: dateNow
            }
        },
        sorts: [
            {	
                "property": "progress_calculate",
                "direction": "ascending"
            }
        ]
    });
    const results = response.results;
    let countTask = 0;
    for (const page of results) {
        countTask++;
        const properties = page.properties;
        //console.log(properties);
        const taskTitle = properties.Description.title[0].plain_text;
        const progress = properties.progress_calculate.formula.number;
        const customer = properties.customer.select.name;
        const stopper = properties.Stopper.checkbox;
        let stopperInfo = '';
        if(stopper){
            const stopperObservation = properties.Stopper_Observations.rich_text[0].plain_text;
            stopperInfo = ` - Stopper 🛑 - (${stopperObservation}) \n`;
        }
        const rowTask = `${countTask}. ${taskTitle}(${progress}%) - ${customer} ${stopper ? stopperInfo  : ''}`;
        task.push(rowTask);
    }
    console.log(task);
    fs.writeFile(pathExport,task.join('\n'), err=>{
        if(err){
            console.log(err);
        }
        console.log("Archivo creado");
    });
})()